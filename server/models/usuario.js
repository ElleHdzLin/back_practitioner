var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var usuarioSchema = new Schema({
    nombre: {type: String, required: [true, 'El nombre es necesario']},
    apellido: {type: String},
    telefono: {type: Number, required: [true, 'El nombre es necesario']},
    email: {type: String, unique: true, required: [true, 'El email es necesario']}
    }); 

module.exports = mongoose.model('usuario', usuarioSchema);