var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator')

var Schema = mongoose.Schema;

var usuarioRegistradoSchema = new Schema({
    id_usuario: {type: String},
    nombre: {type: String},
    apellido: {type: String},
    telefono: {type: Number},
    email: {type: String},
    password: {type: String},
    cuenta:{type: Number}
}); 

usuarioRegistradoSchema.plugin(uniqueValidator, {message: '{PATH} debe ser unico'})

module.exports = mongoose.model('usuarioRegistrado', usuarioRegistradoSchema);