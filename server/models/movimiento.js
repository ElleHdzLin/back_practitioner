var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var movimientoSchema = new Schema({
    fecha: {type: Date},
    tipo_movimiento: {type: String, required: [true, 'especifique el movimiento']},
    monto: {type: Number, required: [true, 'Especifique el monto']},
    descripcion: {type: String},
    id_cuenta: {type: Schema.Types.ObjectId, ref:'cuenta', required: true}
}); 

module.exports = mongoose.model('movimiento', movimientoSchema);