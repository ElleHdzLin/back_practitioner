var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var cuentaSchema = new Schema({
    No_cuenta: {type: Number, unique: true, required: [true, 'La cuenta es necesaria']},
    tipo_cuenta: {type: String, required: [true, 'La cuenta es necesaria']},
    tarjeta: ({
        No_tarjeta:{type: Number},
        CVV: {type: Number}
    }),
    monto: {type: Number},
    id_usuario:{type: Schema.Types.ObjectId, ref:'usuario', required: true}
}); 

module.exports = mongoose.model('cuenta', cuentaSchema);