//Requires
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');


//inicializar variables
var app = express();

// Configurar cabeceras y cors
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
})


//BodyParser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


//importar rutas
var usuarioRoutes = require('./routes/usuario');
var loginRoutes = require('./routes/login');
var cuentaRoutes = require('./routes/cuenta');
var usuarioRegistradoRoutes = require('./routes/usuarioRegistrado');
var movimientoRoutes = require('./routes/movimiento');


//Conexión a la base de datos
mongoose.connect('mongodb+srv://Giselle:RJZjIoxKUAr2m01Q@cluster0-8npnj.mongodb.net/test',
                                         {userNewUrlParser: true, useCreateIndex:true},
                                             (err, res)=> {
                                               if(err) throw err;
                                                    console.log("Base de Datos online");
});

//Rutas
app.use('/usuario', usuarioRoutes);
app.use('/usuarioRegistrado', usuarioRegistradoRoutes);
app.use('/login', loginRoutes);
app.use('/cuenta', cuentaRoutes);
app.use('/movimiento', movimientoRoutes);


//Escuchar peticiones
app.listen(9000, () => {
 console.log('Escuchando puerto 9000');

});
