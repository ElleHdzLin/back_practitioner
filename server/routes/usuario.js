var express = require('express');
//var bcrypt = require('bcrypt');


var app = express();

var Usuario = require('../models/usuario');

//Obtener todos los usuarios
app.get('/', (req, res, next) =>{

    Usuario.find({ })
            .exec(
                ( err, usuarios ) => {
                    if(err){
                         return res.status(500).json({
                            ok: false,
                            mensaje: 'error cargando usuarios',
                            errors: err
                        });
                    }

                        res.status(200).json({
                            ok: true,
                            usuarios: usuarios
                });
            })
});

app.post('/', (req, res) =>{
    var body = req.body;

    var usuario = new Usuario({


        nombre: body.nombre,
        apellido: body.apellido,
        telefono: body.telefono,
        email: body.email     
        });

        usuario.save( ( err, usuarioGuardado ) =>{
            if(err){
                return res.status(500).json({
                   ok: false,
                   mensaje: 'error crear usuario',
                   errors: err
               });
            }
            res.status(200).json({
                ok: true,
                usuario: usuarioGuardado
            });
        });
    
});

module.exports = app;