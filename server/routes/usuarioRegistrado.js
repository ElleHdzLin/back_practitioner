var express = require('express');
var bcrypt = require('bcryptjs');

var app = express();

var UsuarioRegistrado = require('../models/usuarioRegistrado');
var Usuario = require('../models/usuario');
var Cuenta = require('../models/cuenta');

//Obtener todos los usuarioRegistrados
app.get('/', (req, res, next) =>{

    UsuarioRegistrado.find({})
            .exec(
                ( err, usuarioRegistrado ) => {
                    if(err){
                         return res.status(500).json({
                            ok: false,
                            mensaje: 'error cargando usuarioRegistrados',
                            errors: err
                        });
                    }
                        res.status(200).json({
                            ok: true,
                            usuarioRegistrado: usuarioRegistrado
                });
            })
});

//Crear un nuevo usuarioRegistrado
app.post('/', (req, res) =>{
    var body = req.body;
 
    Usuario.findOne({ email: body.email }, (err, usuarioBD)=>{
        if(err){
            return res.status(500).json({
               ok: false,
               mensaje: 'error al buscar usuarios',
               errors: err
           });
        }
        //si el email no es igual a ningun email en la colección usuario        
       if(!usuarioBD){
            return res.status(400).json({
             ok: false,
             mensaje: 'Datos invalidos (email)',
             errors: err
            });
        }else{
            var id = usuarioBD._id;
            Cuenta.findOne({ id_usuario: id , tipo_cuenta: 'debito'})
            .exec(
            ( err, cuentas ) => {                
                         
            if( body.cuenta!= cuentas.No_cuenta){
                 return res.status(400).json({
                    ok: false,
                    mensaje: 'Datos invalidos (cuenta)',
                    errors: err
                });
            }
            //console.log(usuarioBD);

            var usuario = new UsuarioRegistrado({
                
                _id:usuarioBD._id,
                nombre: usuarioBD.nombre,
                apellido: usuarioBD.apellido,
                telefono: usuarioBD.telefono,
                cuenta: cuentas.cuenta,
                email: usuarioBD.email,  
                password: bcrypt.hashSync(body.password, 10)    
                });
        
                usuario.save( ( err, usuarioGuardado ) =>{
                    if(err){
                        return res.status(500).json({
                           ok: false,
                           mensaje: 'error crear usuario',
                           errors: err
                       });
                    }
                    res.status(200).json({
                        ok: true,
                        usuario: usuarioGuardado
                    });
                });

            
          });
        }

    });

});

module.exports = app;