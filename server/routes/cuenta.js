var express = require('express');
var jwt = require('jsonwebtoken');
var mdAutentication = require('../../middlewares/autenticacion');
var mongoose = require('mongoose');

var app = express();

var Cuenta = require('../models/cuenta');

//Obtener todas las cuentas
app.get('/', (req, res, next) =>{
    var body = req.body;

    Cuenta.find({ })
            .exec(
                ( err, cuenta ) => {
                    if(err){
                         return res.status(500).json({
                            ok: false,
                            mensaje: 'error cargando usuarios',
                            errors: err
                        });
                    }

                        res.status(200).json({
                            ok: true,
                            cuenta: cuenta
                });
            })
});

//Crear un nueva cuenta
app.post('/crear', (req, res) =>{
    var body = req.body;

    var cuenta = new Cuenta({


        No_cuenta: body.No_cuenta,
        tipo_cuenta: body.tipo_cuenta,
        tarjeta: ({
            nombre: body.nombre,
            No_tarjeta: body.No_tarjeta,
            CVV: body.CVV
        }),
        monto: body.monto,
        id_usuario: body.id_usuario     
        });

        cuenta.save( ( err, cuentaGuardada ) =>{
            if(err){
                return res.status(500).json({
                   ok: false,
                   mensaje: 'error crear cuenta',
                   errors: err
               });
            }
            res.status(200).json({
                ok: true,
                cuenta: cuentaGuardada
            });
        });
    
});



//Obtener todos las cuentas asociadas a un id_usuario

app.post('/', mdAutentication.verificaToken, (req, res) =>{
    var body = req.body;

    console.log(body.id_usuario);

    Cuenta.find({ id_usuario: mongoose.Types.ObjectId(body.id_usuario)})  
            .exec(
                ( err, cuenta ) => {
                    if(err){
                         return res.status(500).json({
                            ok: false,
                            mensaje: 'error cargando usuarios',
                            errors: err
                        });
                    }

                        res.status(200).json({
                            ok: true,
                            cuenta: cuenta,
                });
                console.log(cuenta);
                
            })
});


module.exports = app;