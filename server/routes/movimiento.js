var express = require('express');
var mdAutentication = require('../../middlewares/autenticacion');

var app = express();

var Movimiento = require('../models/movimiento');
var cuenta = require('../models/cuenta');

//Obtener todos los movimientos
app.post('/', mdAutentication.verificaToken, (req, res, next) => {
    var body = req.body;
    var id = body.id_cuenta;

    Movimiento.find({ id_cuenta: id })
        .exec(
            (err, movimiento) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'error cargando movimientos',
                        errors: err
                    });
                }

                res.status(200).json({
                    ok: true,
                    movimiento: movimiento
                });
            })
});

app.post('/transferencia', mdAutentication.verificaToken, (req, res) => {
    var body = req.body;
    var id = body.id_cuenta;

    var movimiento = new Movimiento({


        fecha: body.nombre,
        tipo_movimiento: body.tipo_movimiento,
        monto: body.monto,
        descripcion: body.descripcion,
        id_cuenta: id
    });

    cuenta.findById({ _id: id }, (err, cuentaBD) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'error al buscar usuarios',
                errors: err
            });
        } else {
            if (movimiento.tipo_movimiento == 'transferencia') {
                console.log(movimiento.tipo_movimiento);

                console.log(cuentaBD.monto);
                if (cuentaBD.monto <= 0 || cuentaBD.monto < movimiento.monto) {
                    console.log('saldo insuficiente');
                    return res.status(400).json({
                        mensaje: 'saldo insuficiente'
                    });
                } else {
                    cuenta.findOne({ No_cuenta: body.cuentaTraspaso }).exec((err, cuentaCambiada) => {
                        if (err) {
                            return res.status(500).json({
                                ok: false,
                                mensaje: 'error al buscar numero de cuenta de transferencia',
                                errors: err
                            });
                        }
                        if (!cuentaCambiada) {
                            return res.status(400).json({
                                mensaje: 'Datos invalidos (numero de cuenta)'
                            });
                        }
                        var idCuentaCambiada = cuentaCambiada._id;
                        console.log(idCuentaCambiada);
                        console.log(cuentaCambiada.monto);
                        cuentaCambiada.monto = cuentaCambiada.monto + movimiento.monto;
                        cuentaBD.monto = cuentaBD.monto - movimiento.monto;
                        console.log(cuentaCambiada.monto);
                        console.log(cuentaBD.monto);
                        //Query parameter is used to search the collection.

                        cuenta.updateOne({ _id: id }, { monto: cuentaBD.monto }, (err, collection) => {
                            if (err) throw err;
                            console.log("updated successfully");
                        });
                        cuenta.updateOne({ _id: idCuentaCambiada }, { monto: cuentaCambiada.monto }, (err, collection) => {
                            if (err) throw err;
                            console.log("updated successfully");
                        });
                        movimiento.save((err, movimientoGuardado) => {
                            if (err) {
                                return res.status(500).json({
                                    mensaje: 'error al realizar movimiento'
                                });
                            }
                            res.status(200).json({
                                moviento: movimientoGuardado,
                                SaldoCuentaOrigen:cuentaBD.monto,
                                SaldoCuentaDestino: cuentaCambiada.monto
                            });
                        });//fin save, guarda el movimiento si este fue exitoso
                    });
                }
            } else {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Datos invalidos (el tipo de movimiento no existe)',
                    errors: err
                });
            }

        }
    });

});

app.post('/retiro', mdAutentication.verificaToken, (req, res) => {
    var body = req.body;
    var id = body.id_cuenta;

    var movimiento = new Movimiento({


        fecha: body.nombre,
        tipo_movimiento: body.tipo_movimiento,
        monto: body.monto,
        descripcion: body.descripcion,
        id_cuenta: id
    });

    cuenta.findById({ _id: id }, (err, cuentaBD) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'error al buscar usuarios',
                errors: err
            });
        } else {
            if (movimiento.tipo_movimiento == 'retiro') {
                if (cuentaBD.monto <= 0 || cuentaBD.monto < movimiento.monto) {
                    return res.status(400).json({
                        mensaje: 'saldo insuficiente'
                    });
                } else {
                    cuentaBD.monto = cuentaBD.monto - movimiento.monto;

                    cuenta.updateOne({ _id: id }, { monto: cuentaBD.monto }, (err, collection) => {
                        if (err) throw err;
                        console.log("updated successfully");
                    });
                    movimiento.save((err, movimientoGuardado) => {
                        if (err) {
                            return res.status(500).json({
                                mensaje: 'error al realizar movimiento'
                            });
                        }
                        res.status(200).json({
                            moviento: movimientoGuardado,
                            SaldoCuenta: cuentaBD.monto
                        });
                    });//fin save, guarda el movimiento si este fue exitoso
                }
            }else {
                return res.status(400).json({
                ok: false,
                mensaje: 'Datos invalidos (el tipo de movimiento no existe)',
                errors: err
                });
            }

        }
    });
    
});


module.exports = app;