var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

var SEED = require('../../config/config').SEED;


var app = express();

var Usuario = require('../models/usuarioRegistrado');


app.post('/', (req, res)=>{
    var body = req.body;
    
    Usuario.findOne({ email: body.email }, (err, usuarioBD)=>{
        if(err){
            return res.status(500).json({
               ok: false,
               mensaje: 'error al buscar usuarios',
               errors: err
           });
        }
        console.log(usuarioBD);

        if(!usuarioBD){
            return res.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrectas',
                errors: err
            });
        }

        if( !bcrypt.compareSync(body.password, usuarioBD.password)){
            return res.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrectas',
                errors: err
            });
        }
        //ocultar el pass
        usuarioBD.password = '*****'
        //Crear token
        var token = jwt.sign({ usuario:usuarioBD}, SEED,{expiresIn: 14400});//4 hrs
        
        
        res.status(200).json({
            data: usuarioBD,
            token:token
        });

    });
});


module.exports = app;